
package com.noid.cinevision;

import org.lwjgl.input.Keyboard;

//import info.ata4.minecraft.minema.client.config.MinemaConfig;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.Minecraft;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraft.util.Timer;
import net.minecraft.util.text.TextFormatting;

import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.RenderTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;



import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;

import java.util.ArrayList;
import java.util.List;


import net.minecraft.util.SoundCategory;
import com.noid.cinevision.audio.audioAssetsModule;


@Mod(modid = CineVision.MODID, name = CineVision.NAME, version = CineVision.VERSION)
public class CineVision
{
    public static final String MODID = "cinevision";
    public static final String NAME = "CineVision";
    public static final String VERSION = "0.1";

    private static final float timeSmooth = 0.5f;
    //public static float timeDate = 0;


    public static final String out = "CineVision";


    public final static KeyBinding TIME_KEYSET = new KeyBinding("cinevision.keytime.kset", Keyboard.KEY_PRIOR, out );
    public final static KeyBinding TIME_KEYSET2 = new KeyBinding("cinevision.keytime.kset2", Keyboard.KEY_NEXT, out );


      public final static KeyBinding EASTEREGG1 = new KeyBinding("cinevision.keytime.kegg", Keyboard.KEY_F2, out);



        public final static KeyBinding TIME_0 = new KeyBinding("cinevision.keytime.k0", Keyboard.KEY_NUMPAD0, out );
        public final static KeyBinding TIME_1 = new KeyBinding("cinevision.keytime.k1", Keyboard.KEY_NUMPAD1, out );
        public final static KeyBinding TIME_2 = new KeyBinding("cinevision.keytime.k2", Keyboard.KEY_NUMPAD2, out );
        public final static KeyBinding TIME_3 = new KeyBinding("cinevision.keytime.k3", Keyboard.KEY_NUMPAD3, out );
        public final static KeyBinding TIME_4 = new KeyBinding("cinevision.keytime.k4", Keyboard.KEY_NUMPAD4, out );
        public final static KeyBinding TIME_5 = new KeyBinding("cinevision.keytime.k5", Keyboard.KEY_NUMPAD5, out );
        public final static KeyBinding TIME_6 = new KeyBinding("cinevision.keytime.k6", Keyboard.KEY_NUMPAD6, out );
        public final static KeyBinding TIME_7 = new KeyBinding("cinevision.keytime.k7", Keyboard.KEY_NUMPAD7, out );
        public final static KeyBinding TIME_8 = new KeyBinding("cinevision.keytime.k8", Keyboard.KEY_NUMPAD8, out );
        public final static KeyBinding TIME_9 = new KeyBinding("cinevision.keytime.k9", Keyboard.KEY_NUMPAD9, out );


          @EventHandler
          public void onInit(FMLInitializationEvent evt) {

            ClientRegistry.registerKeyBinding(TIME_KEYSET);
            ClientRegistry.registerKeyBinding(TIME_KEYSET2);
            ClientRegistry.registerKeyBinding(EASTEREGG1);

            ClientRegistry.registerKeyBinding(TIME_0);
            ClientRegistry.registerKeyBinding(TIME_1);
            ClientRegistry.registerKeyBinding(TIME_2);
            ClientRegistry.registerKeyBinding(TIME_3);
            ClientRegistry.registerKeyBinding(TIME_4);
            ClientRegistry.registerKeyBinding(TIME_5);
            ClientRegistry.registerKeyBinding(TIME_6);
            ClientRegistry.registerKeyBinding(TIME_7);
            ClientRegistry.registerKeyBinding(TIME_8);
            ClientRegistry.registerKeyBinding(TIME_9);

            MinecraftForge.EVENT_BUS.register(this);
          }


                @SubscribeEvent
                public void onKeyInput(InputEvent.KeyInputEvent event) {


                  MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
                  World world = server.worlds[0];



                  if(TIME_KEYSET.isPressed()){

                    if (TIME_0.isPressed()) {
                      world.setWorldTime(0);
                      Utils.print("Time set to 0", TextFormatting.GREEN);
                    }
                    if (TIME_1.isPressed()) {
                      world.setWorldTime(1000);
                      Utils.print("Time set to 1000", TextFormatting.GREEN);
                    }
                    if (TIME_2.isPressed()) {
                      world.setWorldTime(2000);
                      Utils.print("Time set to 2000", TextFormatting.GREEN);
                    }
                    if (TIME_3.isPressed()) {
                      world.setWorldTime(3000);
                      Utils.print("Time set to 3000", TextFormatting.GREEN);
                    }
                    if (TIME_4.isPressed()) {
                      world.setWorldTime(4000);
                      Utils.print("Time set to 4000", TextFormatting.GREEN);
                    }
                    if (TIME_5.isPressed()) {
                      world.setWorldTime(5000);
                      Utils.print("Time set to 5000", TextFormatting.GREEN);
                    }
                    if (TIME_6.isPressed()) {
                      world.setWorldTime(6000);
                      Utils.print("Time set to 6000", TextFormatting.GREEN);
                    }
                    if (TIME_7.isPressed()) {
                      world.setWorldTime(7000);
                      Utils.print("Time set to 7000", TextFormatting.GREEN);
                    }
                    if (TIME_8.isPressed()) {
                      world.setWorldTime(8000);
                      Utils.print("Time set to 8000", TextFormatting.GREEN);
                    }
                    if (TIME_9.isPressed()) {
                      world.setWorldTime(9000);
                      Utils.print("Time set to 9000", TextFormatting.GREEN);
                        new audioAssetsModule().playCrashSound();
                    }


              }

                  if (EASTEREGG1.isPressed()) {
                    new audioAssetsModule().playEasterEgg1Sound();
                    Utils.print("I AM A STEGOSAURUS :D", TextFormatting.LIGHT_PURPLE);
                  }
            }
    }
