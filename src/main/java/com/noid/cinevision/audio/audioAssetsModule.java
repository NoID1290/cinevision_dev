package com.noid.cinevision.audio;

import net.minecraft.init.SoundEvents;
import net.minecraft.util.SoundCategory;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.IOUtils;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

  public class audioAssetsModule {

    private final ResourceLocation crashSoundLocation = new ResourceLocation("cinevision", "error_rip.wav");
    private final ResourceLocation easteregg1Location = new ResourceLocation("cinevision", "easteregg_1.wav");


      public void playCrashSound() {
          playSound(crashSoundLocation);
      }

      public void playEasterEgg1Sound() {
          playSound(easteregg1Location);
      }

      public void playSound(ResourceLocation loc) {
          try {
              InputStream is = Minecraft.getMinecraft().getResourceManager().getResource(loc).getInputStream();
              byte[] bytes = IOUtils.toByteArray(is);
              is.close();
              AudioInputStream ais = AudioSystem.getAudioInputStream(new ByteArrayInputStream(bytes));

              Clip clip = AudioSystem.getClip();
              clip.open(ais);
              clip.start();
          } catch(Exception e) {
              e.printStackTrace();
          }
      }


  }
