package com.noid.cinevision.scene;

import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.GameSettings.Options;


public final class TimeModule {


  private static final float timeSmooth = 0.5f;
  public static float timeDate = 0;


  public static void addTime() {
    timeDate += timeSmooth;

  }

  public static void pastTime() {
    timeDate -= timeSmooth;

  }

  public static void timeReset() {
    timeDate = 0;
  }



}
