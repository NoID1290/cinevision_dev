package info.ata4.minecraft.minema.client.config;

public enum VideoResolution {

    DEFAULT(0), HD1080(1080), HD720(720);

    private final int vres;

    private VideoResolution(int vres) {
        this.vres = vres;
    }

    public int snap(int value) {
        return vres == 0 ? value : value - (value % vres);
    }
}
